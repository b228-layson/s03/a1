/* 

1. What is the blueprint where objects are created from?
classes

2. What is the naming convention applied to classes?
Pascal case

3. What keyword do we use to create objects from a class?
this

4. What is the technical term for creating an object from a class?
constructor

5. What class method dictates HOW objects will be created from that class?
constructor

*/

/*  
QUIZ 2

1. Should class methods be included in the class constructor?
true
2. Can class methods be separated by commas?
false
3. Can we update an object’s properties via dot notation?
yes
4. What do you call the methods used to regulate access to an object’s properties?
class methods
5. What does a method need to return in order for it to be chainable?
return this;
*/